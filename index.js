const http = require('http');
const fs=require('fs');
const uuid=require('uuid');
const path=require('path');
const host='localhost';
const port=4000;

const server=http.createServer((req,res)=>{
if(req.url==="/html" && req.method=="GET"){
    res.setHeader('content-type', 'text/html');
fs.readFile(path.join(__dirname,"index.html"),"utf-8",(err,data)=>{
    if(err){
  res.writeHead(404, 'you get error');
    return  res.end("you get  some error man");
    }else{
        res.writeHead(200, 'text/html');
      return  res.end(data);
    }
})
}else if(req.url==="/json" && req.method=="GET"){
    res.setHeader('content-type', 'application/json');
    fs.readFile(path.join(__dirname,"index.json"),"utf-8",(err,data)=>{
        if(err){
            res.writeHead(404, 'you get error');
           return res.end("somthing is wrong");
        }else{
            res.writeHead(200, 'application/html');
         return   res.end(data);
        }
    })
}else if(req.url==="/uuid" && req.method=="GET"){
    res.setHeader('content-type', 'application/json');
   return res.end(JSON.stringify(`{uuid : ${uuid.v4()}}`));
}else if(req.url.includes("/status/") && req.method=="GET"){
    res.setHeader('content-type', 'application/json');
    let statusarray=req.url.split("/");
    let statuscode=parseInt(statusarray[statusarray.length-1]);
    if(typeof(statuscode) === 'number'){
      return  res.end(`${statuscode} ,${http.STATUS_CODES[statuscode]}`)
    }   
}else if(req.url.includes("/delay/") && req.method=="GET"){
    res.setHeader('content-type', 'application/json');
    let delayarray=req.url.split("/");
    let statuscode=200;
    let delaytime=delayarray[delayarray.length-1];
    
    setTimeout(()=>{
        res.write(`${statuscode},${http.STATUS_CODES[statuscode]}`);
      return  res.end();

    },delaytime*1000)
}else{
    return res.end("enter valid url");
}

}).listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});